package org.inditex.prices.controller;

import java.util.Date;
import java.util.Optional;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.inditex.prices.bean.PriceApplyRequest;
import org.inditex.prices.bean.PriceApplyResponse;
import org.inditex.prices.domain.PriceEntity;
import org.inditex.prices.exception.NoDataFoundException;
import org.inditex.prices.exception.bean.ErrorResponse;
import org.inditex.prices.service.PricesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prices")
public class PricesController
{
    @Autowired
    private PricesService pricesService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "La petición se ha realizado correctamente.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = PriceApplyResponse.class))
            ),
            @ApiResponse(responseCode = "400",
                    description = "Los argumentos recibidos no son correctos",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
            ),
            @ApiResponse(responseCode = "404",
                    description = "No se han encontrado datos que coincidan con el filtro.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
            )}

    )
    @Operation(summary = "Busca la tarifa con máxima prioridad que se debe aplicar a un producto de una marca en una fecha determinada.")
    @GetMapping("/brand/{brandId}/product/{productId}/date/{date}")
    public ResponseEntity<PriceApplyResponse> pricesToApply(@PathVariable("brandId")
                                            @Parameter(description = "Identificador de la cadena del grupo") final int brandId,
                                                           @PathVariable("productId")
                                            @Parameter(description = "Identificador código de producto") final String productId,
                                                           @PathVariable("date")
                                            @Parameter(description = "Fecha (yyyy-MM-dd HH:mm:ss)") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") final Date date) throws NoDataFoundException
    {

        Optional<PriceEntity> priceApply = pricesService.priceToApply(PriceApplyRequest.builder()
                                                                                       .brandId(brandId)
                                                                                       .productId(productId)
                                                                                       .date(date)
                                                                                       .build());

        if (!priceApply.isPresent()) {
            throw new NoDataFoundException("No se ha encontrado tarifa para aplicar.");
        }

        return new ResponseEntity<>(new PriceApplyResponse(priceApply.get()), HttpStatus.OK);
    }

}
