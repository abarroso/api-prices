package org.inditex.prices.repository;


import java.util.List;

import org.inditex.prices.domain.PriceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PricesRepository extends CrudRepository<PriceEntity, String>
{
    List<PriceEntity> findByProductIdAndBrandId(String productId,
                                                int brandId);
}
