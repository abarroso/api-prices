package org.inditex.prices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"org.inditex.prices"})
@EnableConfigurationProperties
@SpringBootApplication
public class App 
{

        public static void main(String[] args) {
            SpringApplication.run(App.class, args);
        }
}
