package org.inditex.prices.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import org.inditex.prices.domain.PriceEntity;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PriceApplyResponse
{
    @JsonProperty("brand_id")
    int brandId;

    @JsonProperty("start_date")
    Date startDate;

    @JsonProperty("end_date")
    Date endDate;

    @JsonProperty("price_list")
    int priceList;

    String price;

    @JsonProperty("product_id")
    String productId;

    public PriceApplyResponse(PriceEntity priceEntity) {
        this.brandId = priceEntity.getBrandId();
        this.price = priceEntity.getPrice();
        this.priceList = priceEntity.getPriceList();
        this.productId = priceEntity.getProductId();
        this.startDate = priceEntity.getStartDate();
        this.endDate = priceEntity.getEndDate();
    }

}
