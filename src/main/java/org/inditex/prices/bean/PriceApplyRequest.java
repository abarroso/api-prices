package org.inditex.prices.bean;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PriceApplyRequest
{
    @NotBlank
    Date date;

    @NotBlank
    String productId;

    @NotBlank
    int brandId;
}
