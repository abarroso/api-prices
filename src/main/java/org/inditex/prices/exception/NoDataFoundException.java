package org.inditex.prices.exception;

public class NoDataFoundException extends Exception
{
    public NoDataFoundException(String msgError) {
        super(msgError);
    }
}
