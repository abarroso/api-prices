package org.inditex.prices.exception;

import java.time.Instant;

import org.inditex.prices.exception.bean.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler
{

    @ExceptionHandler(value = IllegalArgumentException.class)
    protected ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex)
    {
        ErrorResponse errorResponse = ErrorResponse.builder()
                                                   .code(HttpStatus.BAD_REQUEST.value())
                                                   .message(HttpStatus.BAD_REQUEST.name())
                                                   .description(ex.getMessage())
                                                   .date(Instant.now())
                                                   .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoDataFoundException.class)
    protected ResponseEntity<ErrorResponse> handleNoDataFoundException(NoDataFoundException ex)
    {
        ErrorResponse errorResponse = ErrorResponse.builder()
                                                   .code(HttpStatus.NOT_FOUND.value())
                                                   .message(HttpStatus.NOT_FOUND.name())
                                                   .description(ex.getMessage())
                                                   .date(Instant.now())
                                                   .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

}
