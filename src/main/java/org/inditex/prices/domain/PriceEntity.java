package org.inditex.prices.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@Entity
@Table(name = "prices")
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PriceEntity
{

    @Id
    @GeneratedValue
    String id;

    @Schema(description = "Identificador de la cadena del grupo")
    @Column(name = "brand_id")
    int brandId;

    @Schema(description = "Fecha de comienzo en la que aplica el precio tarifa indicado")
    @Column(name = "start_date")
    Date startDate;

    @Schema(description = "Fecha de fin en la que aplica el precio tarifa indicado")
    @Column(name = "end_date")
    Date endDate;

    @Schema(description = "Identificador de la tarifa de precios aplicable")
    @Column(name = "price_list")
    int priceList;

    @Schema(description = "Identificador código de producto")
    @Column(name = "product_id")
    String productId;

    @Schema(description = "Desambiguador de aplicación de precios")
    int priority;

    @Schema(description = "Precio final de venta")
    String price;

    @Schema(description = "Iso de la moneda")
    String curr;
}
