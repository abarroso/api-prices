package org.inditex.prices.service;

import java.util.Optional;

import org.inditex.prices.bean.PriceApplyRequest;
import org.inditex.prices.domain.PriceEntity;

public interface PricesService
{

    /**
     * Busca tarifa para una marca y producto que pueda aplicar entre un rango de fechas. En caso de haber dos o más
     * tarifas que coincidan con la búsqueda se devolverá la de mayor prioridad.
     *
     * @param priceApplyRequest
     * @return price
     */
    Optional<PriceEntity> priceToApply(PriceApplyRequest priceApplyRequest);

}
