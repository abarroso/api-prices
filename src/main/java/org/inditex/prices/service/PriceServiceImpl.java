package org.inditex.prices.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.inditex.prices.bean.PriceApplyRequest;
import org.inditex.prices.domain.PriceEntity;
import org.inditex.prices.repository.PricesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PriceServiceImpl implements PricesService
{

    @Autowired
    private PricesRepository pricesRepository;

    @Override
    public Optional<PriceEntity> priceToApply(PriceApplyRequest priceApplyRequest)
    {
        List<PriceEntity> prices = pricesRepository.findByProductIdAndBrandId(priceApplyRequest.getProductId(),
                                                                              priceApplyRequest.getBrandId()
                                                                             );

        return prices.stream()
                     .filter(price -> price.getStartDate().compareTo(priceApplyRequest.getDate()) < 0
                             && price.getEndDate().compareTo(priceApplyRequest.getDate()) > 0)
                     .max(Comparator.comparing(PriceEntity::getPriority));

    }

}
