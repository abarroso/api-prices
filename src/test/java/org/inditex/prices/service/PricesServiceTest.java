package org.inditex.prices.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

import org.inditex.prices.bean.PriceApplyRequest;
import org.inditex.prices.domain.PriceEntity;
import org.inditex.prices.repository.PricesRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class PricesServiceTest
{

    @InjectMocks
    private PriceServiceImpl pricesService;

    @Mock
    private PricesRepository pricesRepository;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void pricesToApplyOk_WhenPriceRequestMatchWithBD_ShouldReturnPrice() throws ParseException
    {
        PriceEntity priceEntity = PriceEntity.builder()
                                             .price("23.50")
                                             .productId("12345")
                                             .brandId(1)
                                             .priceList(5)
                                             .priority(10)
                                             .curr("EUR")
                                             .startDate(sdf.parse("2020-01-01 10:00:00"))
                                             .endDate(sdf.parse("2020-12-01 10:00:00")).build();

        PriceApplyRequest priceApplyRequest = PriceApplyRequest.builder()
                                                               .productId("12345")
                                                               .brandId(1)
                                                               .date(sdf.parse("2020-05-01 10:00:00")).build();


        Mockito.when(pricesRepository.findByProductIdAndBrandId("12345",1)).thenReturn(Arrays.asList(priceEntity));

        Optional<PriceEntity> optionalPrice = pricesService.priceToApply(priceApplyRequest);

        Assert.assertTrue(optionalPrice.isPresent());
        Assert.assertEquals("23.50", optionalPrice.get().getPrice());

    }

    @Test
    void pricesToApplyKO_WhenPriceRequestNotMatchWithBD_ShouldNotReturnPrice() throws ParseException
    {

        PriceEntity priceEntity = PriceEntity.builder()
                                             .price("23.50")
                                             .productId("12345")
                                             .brandId(1)
                                             .priceList(5)
                                             .priority(10)
                                             .curr("EUR")
                                             .startDate(sdf.parse("2020-01-01 10:00:00"))
                                             .endDate(sdf.parse("2020-02-01 10:00:00")).build();

        PriceApplyRequest priceApplyRequest = PriceApplyRequest.builder()
                                                               .productId("12345")
                                                               .brandId(1)
                                                               .date(sdf.parse("2020-06-01 10:00:00")).build();


        Mockito.when(pricesRepository.findByProductIdAndBrandId("12345",1)).thenReturn(Arrays.asList(priceEntity));

        Optional<PriceEntity> optionalPrice = pricesService.priceToApply(priceApplyRequest);

        Assert.assertFalse(optionalPrice.isPresent());

    }
}
