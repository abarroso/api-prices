package org.inditex.prices.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class PricesControllerTest
{

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    static void setUp()
    {
    }

    @AfterEach
    void tearDown()
    {
    }


    @Test
    void requestTime_2020_06_14_10_00_Product_35455_Brand_1() throws Exception
    {

        mockMvc.perform(MockMvcRequestBuilders.get("/prices/brand/{brandId}/product/{productId}/date/{date}",
                                                   "1",
                                                   "35455",
                                                   "2020-06-14 10:00:00"
                                                  ))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers
                                  .status()
                                  .isOk())
               .andExpect(MockMvcResultMatchers
                                  .content()
                                  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.jsonPath("$.price_list").value(1));
    }

    @Test
    void requestTime_2020_06_14_16_00_Product_35455_Brand_1() throws Exception
    {

        mockMvc.perform(MockMvcRequestBuilders.get("/prices/brand/{brandId}/product/{productId}/date/{date}",
                                                   "1",
                                                   "35455",
                                                   "2020-06-14 16:00:00"
                                                  ))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers
                                  .status()
                                  .isOk())
               .andExpect(MockMvcResultMatchers
                                  .content()
                                  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.jsonPath("$.price_list").value(2));
    }

    @Test
    void requestTime_2020_06_14_21_00_Product_35455_Brand_1() throws Exception
    {

        mockMvc.perform(MockMvcRequestBuilders.get("/prices/brand/{brandId}/product/{productId}/date/{date}",
                                                   "1",
                                                   "35455",
                                                   "2020-06-14 21:00:00"
                                                  ))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers
                                  .status()
                                  .isOk())
               .andExpect(MockMvcResultMatchers
                                  .content()
                                  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.jsonPath("$.price_list").value(1));
    }

    @Test
    void requestTime_2020_06_15_10_00_Product_35455_Brand_1() throws Exception
    {

        mockMvc.perform(MockMvcRequestBuilders.get("/prices/brand/{brandId}/product/{productId}/date/{date}",
                                                   "1",
                                                   "35455",
                                                   "2020-06-15 10:00:00"
                                                  ))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers
                                  .status()
                                  .isOk())
               .andExpect(MockMvcResultMatchers
                                  .content()
                                  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.jsonPath("$.price_list").value(3));
    }

    @Test
    void requestTime_2020_06_16_21_00_Product_35455_Brand_1() throws Exception
    {

        mockMvc.perform(MockMvcRequestBuilders.get("/prices/brand/{brandId}/product/{productId}/date/{date}",
                                                   "1",
                                                   "35455",
                                                   "2020-06-16 21:00:00"
                                                  ))
               .andDo(MockMvcResultHandlers.print())
               .andExpect(MockMvcResultMatchers
                                  .status()
                                  .isOk())
               .andExpect(MockMvcResultMatchers
                                  .content()
                                  .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.jsonPath("$.price_list").value(4));
    }

}
