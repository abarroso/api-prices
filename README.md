💵 api-prices
==============

### 📄 Descripción
API que contiene el CRUD (no implementados los endpoints) para la tabla PRICES. Además, dispone de un endpoint que nos
permite obtener la tarifa a aplicar según unos criterios definidos.

### 🗂️ Arquitectura y carpetas
La API ha sido diseñada y desarrollada en una arquitectura por capas en la que cada capa tiene una única
responsabilidad:

* **Capa de presentación**: esta capa recibe las peticiones de los clientes que consumen el api y representa 
los datos solicitados

* **Capa lógica de negocio**: es la capa intermediaria entre la capa de presentación y la capa de acceso a BD. Maneja la 
funcionalidad de la aplicación, como por ejemplo: reglas, cálculos, lógica, etc..

* **Capa de acceso a BD**: mediante esta capa podremos guardar o recuperar los datos que manejará nuestra aplicación.



* src
    * main
        * java
            * controller - *Capa presentación*
            * service - *Capa lógica de negocio*
            * repository - *Capa de acceso a BD*
        * resources
    * test
        * java - *Tests unitarios y de integración*


### 🛠 Configuración de la base de datos

La base de datos que implementa la API es una base de datos H2 en memoria mediante Springboot. Su configuración se encuentra en el
archivo [application.yml](./src/main/resources/application.yml).

Dispone de una consola que es una interfaz gráfica que se accede desde el navegador. Una vez arranca la API podremos
acceder desde la siguiente url.

http://localhost:8080/h2-console/

La base de datos no mantiene los datos, cada vez que se despliegue la API se inicializará de nuevo el esquema y la
información contenida. El esquema y los datos iniciales se encuentran en los ficheros [schema.sql](./src/main/resources/schema.sql)
y [data.sql](./src/main/resources/data.sql)


### 🚀 Ejecución
Para compilar el proyecto simplemente se debe ejecutar el comando

```
mvn clean package
```

Para desplegar esta API en local deberemos tener instalada la versión 11 de java. Se recomienda usar el IDE IntelliJ.
Los paso para ejecutar en local son:

1. Clonar el respositorio en una carpeta local.

2. Configurar la BBDD desde el archivo application.yml en caso necesario.

3. Arrancar la aplicación desde la clase App.java (Botón derecho sobre la clase -> Run 'App.main()')

4. La aplicación por defecto arrancará en el host 127.0.0.1 en el puerto 8080.

### ✅ Pruebas

La API incluye una serie de test unitarios y de integración en la ruta src/test/java. Estos test deberán ser
ejecutados/modificados siempre que incluyamos o modifiquemos alguna funcionalidad. Además, para realizar pruebas manuales al levantar
la API dispondremos de un swagger documentado donde podremos ejecutar cualquier endpoint y ver la documentación de la api.
El swagger por defecto se levanta en la siguiente dirección:

http://localhost:8080/swagger-ui/index.html

Para establecer el nombre de cada test unitario se utiliza siempre la misma nomenclatura:

nameTest_When....._Should.....

Ejemplo: pricesToApplyOk_WhenPriceRequestMatchWithBD_ShouldReturnPrice

Como excepción los test de integración se han nombrado con otra nomenclatura para hacer referencia a las pruebas solicitadas
en esta prueba.

### ✴️ Manejo de excepciones

Para personalizar las excepciones se han implementado @RestControllerAdvice, así podremos capturar las excepciones que
nosotros deseemos y personalizar el mensaje que devuelven.

### ➡️ Lista de cambios

Se puede consultar los cambios realizados por versión en el siguiente documento: [CHANGELOG](./CHANGELOG.md)